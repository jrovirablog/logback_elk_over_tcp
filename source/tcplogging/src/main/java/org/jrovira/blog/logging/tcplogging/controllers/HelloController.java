package org.jrovira.blog.logging.tcplogging.controllers;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.jboss.logging.MDC;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.Marker;
import org.slf4j.helpers.BasicMarkerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import net.logstash.logback.argument.StructuredArguments;

@RestController
public class HelloController {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(HelloController.class);
	
	@GetMapping("/hello")
	public String hello() {
		MDC.put("mdc_field", UUID.randomUUID().toString());
		
		LOGGER.info("Log simple");

		Marker marker = new BasicMarkerFactory().getMarker("test_marker");
		marker.add(new BasicMarkerFactory().getMarker("test_marker2"));
		marker.add(new BasicMarkerFactory().getMarker("test_marker3"));
		LOGGER.info(marker, "Log con markers");
		
		LOGGER.info("Log con un argumento", StructuredArguments.value("arg1", "arg1value"));
		
		Map<Object, Object> map = new HashMap<Object, Object>();
		map.put("arg1", "arg1 with map");
		map.put("arg2", "arg2 with map");
		map.put("arg3", "arg3 with map");
		LOGGER.info("Log con múltiples argumentos", StructuredArguments.entries(map));
		
		return "hello";
	}

}
