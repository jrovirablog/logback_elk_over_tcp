package org.jrovira.blog.logging.tcplogging;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TcploggingApplication {

	public static void main(String[] args) {
		SpringApplication.run(TcploggingApplication.class, args);
	}

}

